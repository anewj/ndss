Webapp for **Down Syndrome Society, Nepal**

Technologies:
1. Grails v_2.5.2 (Java framework)
2. AngularJs v_1.5.5 (Javascript framework)
3. Bootstrap v_3.3.6 (CSS framework)
4. Java JDK(8.0_45) >= recommended (Java virtual Machine)

This app uses 
REST API and uses MySql as database.


____________________________________________________________
Contributors :
Anewj Maharjan,
Shreenu Shrestha