class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/$controller"{
            action = [GET:"index",POST: "save",DELETE:"delete"]
            constraints {
            }
        }

        "/"(controller: 'dashboard')
        "500"(view:'/error')
	}
}
