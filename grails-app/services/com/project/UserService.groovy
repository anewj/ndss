package com.project

import grails.transaction.Transactional

class UserService {
    @Transactional
    def save(Users userTest) {
        return userTest.save()
    }

    @Transactional
    def getUsers(){
        return Users.all
    }
}
