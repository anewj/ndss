package com.project

class Users {
    String firstName
    String middleName
    String lastName

    static constraints = {
        firstName nullable: false
        lastName nullable: false
        middleName blank: true, nullable: true
    }
}
