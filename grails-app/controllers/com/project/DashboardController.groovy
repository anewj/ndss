package com.project

import org.springframework.http.HttpStatus

class DashboardController {
    static responseFormats = ['json', 'xml']
    static allowedMethods = [users: 'GET', save: 'POST', delete: 'DELETE']
    def userService


    def index() {}

    def save() {
        println("hello")
        Users userTest = new Users()
        bindData userTest, request
        userTest.validate()
        if (userTest.hasErrors()) {
            respond userTest.errors, status: HttpStatus.UNPROCESSABLE_ENTITY
            return
        }
        respond userService.save(userTest)
    }

    def users() {
        respond userService.users
    }
}
